package ru.romadro.whoosh.api

import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.romadro.whoosh.BuildConfig
import ru.romadro.whoosh.api.dto.GetInfo

class RemoteDataSource {

    private val service by lazy {
        Retrofit.Builder()
            .baseUrl(PROD_HOST)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(
                OkHttpClient()
                    .newBuilder()
                    .addInterceptor {
                        it.proceed(
                            it.request().newBuilder()
                                .addHeader(API_KEY_HEADER_NAME, BuildConfig.API_KEY)
                                .build()
                        )
                    }
                    .build()
            )
            .build()
            .create(WhooshService::class.java)
    }

    fun getInfo(code: String): Single<GetInfo> = service.getInfo(code)

    private companion object {
        private const val API_KEY_HEADER_NAME = "x-api-key"
        private const val PROD_HOST = "https://api.whoosh.bike"
    }
}