package ru.romadro.whoosh.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.romadro.whoosh.api.dto.GetInfo


interface WhooshService {
    @GET("/challenge/getinfo")
    fun getInfo(@Query("code") code: String): Single<GetInfo>
}