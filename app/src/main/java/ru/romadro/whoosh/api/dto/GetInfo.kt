package ru.romadro.whoosh.api.dto

data class GetInfo(
    val status: Status?,
    val comments: String?
) {
    enum class Status {
        CHANGE_BATTERY
    }
}