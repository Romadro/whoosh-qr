package ru.romadro.whoosh.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.zxing.integration.android.IntentIntegrator
import ru.romadro.whoosh.databinding.MainFragmentBinding
import ru.romadro.whoosh.utils.Event.Companion.observeEvents
import ru.romadro.whoosh.utils.ScanResult
import ru.romadro.whoosh.utils.setOnTextChangedListener

class MainFragment : Fragment() {

    private var binding: MainFragmentBinding? = null

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(
            this,
            MainViewModelFactory(
                url = arguments?.getString(URL_ARG_KEY),
                context = requireContext().applicationContext
            )
        ).get(MainViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        MainFragmentBinding.inflate(inflater, container, false)
            .also { binding = it }
            .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding!!) {
            scanButton.setOnClickListener { viewModel.onScanClick() }
            scooterCodeInputEditText.setOnTextChangedListener(viewModel::onScooterCodeChanged)
            retryButton.setOnClickListener { viewModel.onRetry() }
        }

        with(viewModel) {
            scanEventLiveData.observeEvents(viewLifecycleOwner, ::startScanQr)
            scooterCodeLiveData.observe(viewLifecycleOwner, binding!!.scooterCode::setText)
            scooterStatusLiveData.observe(viewLifecycleOwner, binding!!.status::setText)
            retryButtonVisibilityLiveData.observe(viewLifecycleOwner) { visible ->
                binding!!.retryButton.visibility = if (visible) View.VISIBLE else View.GONE
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            viewModel.onScanResult(
                result.contents
                    ?.let(ScanResult::Success)
                    ?: ScanResult.Cancelled
            )
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    fun onNewUrl(url: String) {
        viewModel.onNewUrlReceived(url)
    }

    private fun startScanQr() {
        IntentIntegrator
            .forSupportFragment(this)
            .setOrientationLocked(false)
            .initiateScan()
    }

    companion object {
        private const val URL_ARG_KEY = "URL"

        fun newInstance(url: String?) = MainFragment()
            .apply {
                arguments = Bundle()
                    .apply {
                        url?.let { putString(URL_ARG_KEY, it) }
                    }
            }
    }
}