package ru.romadro.whoosh.ui.main

import io.reactivex.Single
import ru.romadro.whoosh.utils.QrCodeUriParseException
import java.net.URI

class ScooterCodeExtractor {

    fun parseScooterCode(url: String): Single<String> =
        Single.create { emitter ->
            runCatching { URI.create(url) }
                .getOrNull()
                ?.let { uri ->
                    uri.queryAsMap
                        .takeIf { it.isNotEmpty() }
                        ?.let { queryAsMap ->
                            queryAsMap[CODE_QUERY_KEY]
                                ?.let { emitter.onSuccess(it) }
                                ?: emitter.onError(
                                    QrCodeUriParseException("uri = $url, code query key not found")
                                )
                        }
                        ?: emitter.onError(QrCodeUriParseException("uri = $url, query is empty"))
                }
                ?: emitter.onError(QrCodeUriParseException("uri = $url"))
        }

    private val URI.queryAsMap: Map<String, String>
        get() = query
            .split("&")
            .mapNotNull {
                it.split("=")
                    .takeIf { it.size == 2 }
                    ?.let { it[0] to it[1] }
            }
            .toMap()

    private companion object {
        private const val CODE_QUERY_KEY = "scooter_code"
    }
}