package ru.romadro.whoosh.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Notification
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import ru.romadro.whoosh.R
import ru.romadro.whoosh.utils.Event
import ru.romadro.whoosh.utils.QrCodeUriParseException
import ru.romadro.whoosh.utils.ScanResult
import ru.romadro.whoosh.utils.Schedulers
import ru.romadro.whoosh.utils.StringProvider
import java.io.IOException
import java.util.concurrent.TimeUnit

class MainViewModel(
    private val url: String?,
    private val scooterCodeExtractor: ScooterCodeExtractor,
    private val repository: MainRepository,
    private val schedulers: Schedulers,
    private val stringProvider: StringProvider
) : ViewModel() {

    val scanEventLiveData: LiveData<Event>
        get() = _scanEventLiveData
    val scooterCodeLiveData: LiveData<String>
        get() = _scooterCodeLiveData
    val scooterStatusLiveData: LiveData<String>
        get() = _scooterStatusLiveData
    val retryButtonVisibilityLiveData: LiveData<Boolean>
        get() = _retryButtonVisibilityLiveData

    private val _scanEventLiveData = MutableLiveData<Event>()
    private val _scooterCodeLiveData = MutableLiveData<String>()
    private val _scooterStatusLiveData = MutableLiveData<String>()
    private val _retryButtonVisibilityLiveData = MutableLiveData(false)

    private var disposable: Disposable? = null

    private var urlSubject = PublishSubject.create<String>()
    private var codeSubject = PublishSubject.create<String>()

    private var retrySubject = PublishSubject.create<Unit>()

    init {
        disposable = scooterCodeObservable()
            .repeatOn(retrySubject)
            .switchMap {
                when {
                    it.isOnNext -> repository.getInfo(it.value!!)
                        .doOnSubscribe {
                            _scooterStatusLiveData.postValue(stringProvider.getString(R.string.loading))
                        }
                        .subscribeOn(schedulers.io)
                        .toObservable()
                        .materialize()
                    it.isOnError -> Observable.just(Notification.createOnError(it.error!!))
                    else -> Observable.just(Notification.createOnComplete())
                }
            }
            .doOnNext {
                when {
                    it.isOnNext -> {
                        _retryButtonVisibilityLiveData.postValue(false)
                        _scooterStatusLiveData.postValue(it.value?.comments.orEmpty())
                    }
                    it.isOnError -> {
                        it.error!!.getErrorState()
                            .let { (errorMessage, retryVisible) ->
                                _retryButtonVisibilityLiveData.postValue(retryVisible)
                                _scooterStatusLiveData.postValue(errorMessage)
                            }
                    }
                }
            }
            .subscribe()
    }

    fun onScanClick() {
        _scanEventLiveData.value = Event()
    }

    fun onScooterCodeChanged(input: String) {
        codeSubject.onNext(input)
    }

    fun onNewUrlReceived(url: String) {
        urlSubject.onNext(url)
    }

    fun onScanResult(scanResult: ScanResult) {
        when (scanResult) {
            is ScanResult.Success -> {
                urlSubject.onNext(scanResult.contents)
            }
            is ScanResult.Cancelled -> {
                // do nothing
            }
        }
    }

    fun onRetry() {
        retrySubject.onNext(Unit)
    }

    override fun onCleared() {
        disposable?.dispose()
        disposable = null
        super.onCleared()
    }

    private fun scooterCodeObservable(): Observable<Notification<String>> =
        Observable.merge(
            urlSubject
                .mergeWith(
                    Observable.create { emitter ->
                        url?.let { emitter.onNext(it) }
                            ?: emitter.onComplete()
                    }
                )
                .switchMap { url ->
                    scooterCodeExtractor.parseScooterCode(url)
                        .toObservable()
                        .materialize()
                },
            codeSubject
                .debounce(1000, TimeUnit.MILLISECONDS, schedulers.computation)
                .materialize()
        )
            .map {
                when {
                    it.isOnNext -> Notification.createOnNext(it.value!!.trim().uppercase())
                    else -> it
                }
            }
            .filter {
                it.isOnError || (it.isOnNext && it.value!!.isNotEmpty() && it.value!!.isNotBlank())
                // && code.length == 4 // not sure about it
            }
            .doOnNext {
                when {
                    it.isOnNext -> {
                        _scooterCodeLiveData.postValue(it.value)
                        _retryButtonVisibilityLiveData.postValue(false)
                        _scooterStatusLiveData.postValue("")
                    }
                    it.isOnError -> {
                        _scooterCodeLiveData.postValue("")
                        it.error!!.getErrorState()
                            .let { (errorMessage, retryVisible) ->
                                _retryButtonVisibilityLiveData.postValue(retryVisible)
                                _scooterStatusLiveData.postValue(errorMessage)
                            }
                    }
                }
            }

    private fun Throwable.getErrorState() =
        when (this) {
            is SecurityException -> R.string.permission_error to false
            is QrCodeUriParseException -> R.string.qr_code_error to false
            is IOException -> R.string.network_error to true
            else -> R.string.unknown_error to false
        }
            .let { (errorStringResId, retryVisibility) ->
                stringProvider.getString(errorStringResId) to retryVisibility
            }

    private companion object {
        private fun <T> Observable<T>.repeatOn(observable: Observable<Unit>): Observable<T> =
            Observable.combineLatest(
                observable
                    .startWith(Unit),
                this,
                { _, value -> value }
            )
    }
}