package ru.romadro.whoosh.ui.main

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.romadro.whoosh.api.RemoteDataSource
import ru.romadro.whoosh.utils.AndroidSchedulers
import ru.romadro.whoosh.utils.AndroidStringProvider

class MainViewModelFactory(
    private val context: Context,
    private val url: String?
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        MainViewModel(
            url = url,
            scooterCodeExtractor = ScooterCodeExtractor(),
            repository = MainRepository(
                remote = RemoteDataSource()
            ),
            schedulers = AndroidSchedulers(),
            stringProvider = AndroidStringProvider(context)
        ) as T
}