package ru.romadro.whoosh.ui.main

import ru.romadro.whoosh.api.RemoteDataSource

class MainRepository(private val remote: RemoteDataSource) {
    fun getInfo(code: String) = remote.getInfo(code)
}