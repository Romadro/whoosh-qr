package ru.romadro.whoosh.utils

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface Schedulers {
    val io: Scheduler
    val ui: Scheduler
    val computation: Scheduler
}

class AndroidSchedulers : ru.romadro.whoosh.utils.Schedulers {
    override val io: Scheduler
        get() = Schedulers.io()
    override val ui: Scheduler
        get() = AndroidSchedulers.mainThread()
    override val computation: Scheduler
        get() = Schedulers.computation()
}