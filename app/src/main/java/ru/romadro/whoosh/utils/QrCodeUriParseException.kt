package ru.romadro.whoosh.utils

import java.lang.Exception

class QrCodeUriParseException(message: String) : Exception(message)