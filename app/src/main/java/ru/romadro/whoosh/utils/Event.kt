package ru.romadro.whoosh.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

class Event {

    var handled = false
        private set

    private fun handle(): Boolean =
        if (!handled) {
            handled = true
            true
        } else {
            false
        }

    companion object {
        fun LiveData<Event>.observeEvents(owner: LifecycleOwner, observer: () -> Unit) =
            observe(owner) {
                if (it.handle()) {
                    observer.invoke()
                }
            }
    }
}