package ru.romadro.whoosh.utils

sealed class ScanResult {
    data class Success(val contents: String): ScanResult()
    object Cancelled: ScanResult()
}