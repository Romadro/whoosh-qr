package ru.romadro.whoosh

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.romadro.whoosh.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    private val currentFragment: MainFragment?
        get() = supportFragmentManager.findFragmentById(R.id.container) as? MainFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.container,
                    MainFragment.newInstance(intent.dataString)
                )
                .commitNow()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.url?.let { currentFragment?.onNewUrl(it) }
    }
}

private val Intent.url: String?
    get() = dataString